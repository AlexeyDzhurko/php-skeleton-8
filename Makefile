include .env

all: | ${APP_ENV}
local: | docker-build docker-up composer-install
stage: | docker-build docker-up composer-install
prod: | docker-build docker-up composer-install

##### composer #####

composer-install:
	docker-compose exec php-fpm composer install

##### application #####

run-migration:
	docker-compose exec php-fpm php artisan migrate

run-migration-seed:
	docker-compose exec php-fpm php artisan migrate --seed

##### docker compose #####
docker-build:
	docker-compose build

docker-up:
	docker-compose up -d

docker-rebuild:
	docker-compose down \
	&& docker-compose up -d --build \
	&& docker-compose exec php-fpm composer install
